package main

import (
	"encoding/json"
	"flag"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"sort"
	"time"
)

type News struct {
	ID            int       `json:"id"`
	Title         string    `json:"title"`
	Body          string    `json:"body"`
	Provider      string    `json:"provider"`
	PublishedTime time.Time `json:"published_at"`
	Tickers       []string  `json:"tickers"`
}

type Feed struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}

type CompanyNews struct {
	Items         []News    `json:"items"`
	PublishedTime time.Time `json:"published_at"`
	Tickers       []string  `json:"tickers"`
}

func readNews(name string) ([]News, error) {
	data, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, errors.Wrapf(err, "file is not exist %s", name)
	}
	var slice []News
	err = json.Unmarshal(data, &slice)
	if err != nil {
		return nil, errors.Wrap(err, "can't unmarshal data")
	}
	return slice, nil

}

func checkTickers(t1 []string, t2 []string) bool {

	if len(t1) != len(t2) {
		return false
	}
	m1 := make(map[string]bool)

	for _, el := range t1 {
		m1[el] = true
	}

	for _, el := range t2 {
		if !m1[el] { // Проверять наличие элемента не требуется, т.к. нас утраивает zero value
			return false
		}
	}

	return true
}

func tryToAddToCompany(j News, comData [][]News) ([][]News, bool) {
	for i, k := range comData {
		if checkTickers(j.Tickers, k[0].Tickers) && j.PublishedTime.Year() == k[0].PublishedTime.Year() && j.PublishedTime.Month() == k[0].PublishedTime.Month() && j.PublishedTime.Day() == k[0].PublishedTime.Day() { //Добавляем в существующую компанию
			comData[i] = append(comData[i], j)
			return comData, true
		}
	}
	return comData, false
}

func tryToCreateCompany(j News, comData [][]News, data []News) ([][]News, bool) {
	for _, k := range data {
		if checkTickers(j.Tickers, k.Tickers) && j.ID != k.ID && j.PublishedTime.Year() == k.PublishedTime.Year() && j.PublishedTime.Month() == k.PublishedTime.Month() && j.PublishedTime.Day() == k.PublishedTime.Day() { //Создаем компанию
			var temp []News
			temp = append(temp, j)
			comData = append(comData, temp)
			return comData, true
		}
	}
	return comData, false
}

func separateNews(data []News) ([][]News, []News) {

	var comData [][]News
	var newsData []News

	var fl bool

	for _, el := range data {
		comData, fl = tryToAddToCompany(el, comData)
		if fl {
			continue
		}


		comData, fl = tryToCreateCompany(el, comData, data)
		if fl {
			continue
		}
		newsData = append(newsData, el)
	}
	return comData, newsData
}

func mergeNews(comData [][]News, newsData []News) []Feed {

	var i, j int
	var temp Feed
	var feedOut []Feed
	for i < len(comData) || j < len(newsData) {
		if i == len(comData) || newsData[j].PublishedTime.Before(comData[i][0].PublishedTime) {
			temp = Feed{
				Type:    "news",
				Payload: newsData[j],
			}
			feedOut = append(feedOut, temp)
			j += 1
			continue
		}
		temp = Feed{
			Type: "company_news",
			Payload: CompanyNews{
				Tickers:       comData[i][0].Tickers,
				PublishedTime: comData[i][0].PublishedTime,
				Items:         comData[i],
			},
		}
		feedOut = append(feedOut, temp)

		i += 1
	}
	return feedOut
}

func printToJsonFile(itog []Feed) error {
	jjson, err := json.MarshalIndent(itog, "", "    ")

	if err != nil {
		return errors.Wrap(err, "json marshaling failed")

	}

	err = ioutil.WriteFile("out.json", jjson, 0644)
	if err != nil {
		return errors.Wrap(err, "cannot write file")
	}
	return nil
}

func main() {

	var name string
	flag.StringVar(&name, "file", "hw2/news.json", "The name of news")
	flag.Parse()

	data, err := readNews(name)
	if err != nil {
		log.Fatal(err)
	}

	comData, newsData := separateNews(data)

	for k := range comData {
		sort.Slice(comData[k], func(i, j int) bool {
			return comData[k][i].PublishedTime.Before(comData[k][j].PublishedTime)
		})
	}

	sort.Slice(comData, func(i, j int) bool { return comData[i][0].PublishedTime.Before(comData[j][0].PublishedTime) })

	sort.Slice(newsData, func(i, j int) bool {
		return newsData[i].PublishedTime.Before(newsData[j].PublishedTime)
	})

	err = printToJsonFile(mergeNews(comData, newsData))
	if err != nil {
		log.Fatal(err)
	}

}
